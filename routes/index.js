var express = require('express');
var router = express.Router();

var user_controller = require('../controllers/UserController'); 

router.get('/', user_controller.sign_in_get);

router.post('/sign_in', user_controller.sign_in_post);

router.get('/sign_up', user_controller.sign_up_get);

router.post('/sign_up', user_controller.sign_up_post);

module.exports = router;
