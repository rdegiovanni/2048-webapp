# 2048-webbapp 
# Requirements * [npm] standard Node.js package manager 
(In Linux, run the following command: sudo apt install npm)
# Installation 
1) Clone the repository: 
$ git clone https://rdegiovanni@bitbucket.org/rdegiovanni/2048-webapp.git
2) Then, run the following commands:
$ cd 2048-webapp
$ npm install 
3) Once that the installation of all dependencies has finished, to execute the application use the following command:
$ npm run devstart
