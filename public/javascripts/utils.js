window.onload = function() {
    //For user validations
    var inputUsername = document.getElementById("username");
    var inputEmail = document.getElementById("email");
    var inputPass = document.getElementById("password");
    var inputConfirmPass = document.getElementById("pass_confirmation");
    var name = document.getElementById("letterNuber");  

    // When the user clicks on the "x" field, show the message box
    inputUsername.onfocus = function() {
        document.getElementById("messageU").style.display = "block";
    }
    inputEmail.onfocus = function() {
        document.getElementById("messageE").style.display = "block";
    }
    inputPass.onfocus = function() {
        document.getElementById("messageP").style.display = "block";
    }
    inputConfirmPass.onfocus = function() {
        document.getElementById("messageCF").style.display = "block";
    }
    
    // When the user starts to type something inside the "username" field
    inputUsername.onkeyup = function() {
      //Character special
      var invalidName = /[`~!@#$%^&*()_°¬|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
      var spacing =/[\s]/g;
      
      //Inicialization
      letterNumber.classList.remove("stylemessage_neutral");
      letterNumber.classList.remove("stylemessage_green");

      //Validate special characters
      if(inputUsername.value.match(invalidName) || inputUsername.value.match(spacing)) {  
        letterNumber.classList.remove("stylemessage_green");
        letterNumber.classList.add("stylemessage_red");
      } else {
        letterNumber.classList.remove("stylemessage_red");
        letterNumber.classList.add("stylemessage_green");
      }

      //Validate number of characters

      //Inicialization
      userLength.classList.remove("stylemessage_neutral");
      userLength.classList.add("stylemessage_red");

      if(inputUsername.value.length>=8) {  
        userLength.classList.remove("stylemessage_red");
        userLength.classList.add("stylemessage_green");
      } else {
        userLength.classList.remove("stylemessage_green");
        userLength.classList.add("stylemessage_red");
      }
    }

    // When the user starts to type something inside the "email" field
    inputEmail.onkeyup = function() {
      //Inicialization
      mail.classList.remove("stylemessage_neutral");
      mail.classList.add("stylemessage_red");
      
      //Validate email
      var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/gi;
      if(inputEmail.value.match(emailRegex)) {
        mail.classList.remove("stylemessage_red");
        mail.classList.add("stylemessage_green");
      } else {
        mail.classList.remove("stylemessage_green");
        mail.classList.add("stylemessage_red");
      }

    }
    // When the user starts to type something inside the "password" field
    inputPass.onkeyup = function() {
      // Validate capital letters
      var upperCaseLetters = /[A-Z]/g;
      //Inicialization
      capital.classList.remove("stylemessage_neutral");
      capital.classList.add("stylemessage_red");

      if(inputPass.value.match(upperCaseLetters)) {  
        capital.classList.remove("stylemessage_red");
        capital.classList.add("stylemessage_green");
      } else {
        capital.classList.remove("stylemessage_green");
        capital.classList.add("stylemessage_red");
      }

      // Validate lowercase letters
      var lowerCaseLetters = /[a-z]/g;
      //Inicialization
      lowercase.classList.remove("stylemessage_neutral");
      lowercase.classList.add("stylemessage_red");

      if(inputPass.value.match(lowerCaseLetters)) {  
        lowercase.classList.remove("stylemessage_red");
        lowercase.classList.add("stylemessage_green");
      } else {
        lowercase.classList.remove("stylemessage_green");
        lowercase.classList.add("stylemessage_red");
      }

      // Validate numbers
      var numbers = /[0-9]/g;
      //Inicialization
      number.classList.remove("stylemessage_neutral");
      number.classList.add("stylemessage_red");

      if(inputPass.value.match(numbers)) {  
        number.classList.remove("stylemessage_red");
        number.classList.add("stylemessage_green");
      } else {
        number.classList.remove("stylemessage_green");
        number.classList.add("stylemessage_red");
      }
      
      // Validate length

      //Inicialization
      character.classList.remove("stylemessage_neutral");
      character.classList.add("stylemessage_red");

      if(inputPass.value.length >= 8) {
        character.classList.remove("stylemessage_red");
        character.classList.add("stylemessage_green");
      } else {
        character.classList.remove("stylemessage_green");
        character.classList.add("stylemessage_red");
      }

      //Validation of the password confirmation in case the password is changed.
      if (inputConfirmPass.value.length !=0) {
        if(inputPass.value == inputConfirmPass.value) {
          matching.classList.remove("stylemessage_red");
          matching.classList.add("stylemessage_green");
        } else {
          matching.classList.remove("stylemessage_green");
          matching.classList.add("stylemessage_red");
        }
      }
    }

    // When the user starts to type something inside the "confirm password" field
    inputConfirmPass.onkeyup = function() {
      //Inicialization
      matching.classList.remove("stylemessage_neutral");
      matching.classList.add("stylemessage_red");

      // Validate matching passwords
      if(inputPass.value.length!=0 && inputPass.value == inputConfirmPass.value) {
        matching.classList.remove("stylemessage_red");
        matching.classList.add("stylemessage_green");
      } else {
        matching.classList.remove("stylemessage_green");
        matching.classList.add("stylemessage_red");
      }
    }
}

// True if all fields are completed correctly
function verification(){

  //Validate username      
  if (userLength.classList.contains("stylemessage_red")) {
    alert("Format invalid Username: Must contain at least 8 characters");
    return false;
  }
  if (letterNumber.classList.contains("stylemessage_red")) { 
    alert("Format invalid Username: It can not contain special characters");  
    return false;
  }
  //Validate email
  if (mail.classList.contains("stylemessage_red")) { 
    alert("Format invalid Email: Remember '@' and '.'");  
    return false;
  }
  //Validate password
  if (capital.classList.contains("stylemessage_red")) {
    alert("Format invalid Password: Must contain at least one uppercase letter");
    return false;
  }
  if (lowercase.classList.contains("stylemessage_red")) {
    alert("Format invalid Password: Must contain at least one lowercase letter");
    return false;
  }
  if (number.classList.contains("stylemessage_red")) {
    alert("Format invalid Password: Must contain at least one number");
    return false;
  }
  if (character.classList.contains("stylemessage_red")) {
    alert("Format invalid Password: Must contain at least 8 characters");
    return false;
  }
  //Validate confirma password
  if (matching.classList.contains("stylemessage_red")) { 
    alert("Passwords do not match");
    return false;
  }
  //Validate empty fields
  if (username.value.length==0 || email.value.length==0 || password.value.length==0 || pass_confirmation.value.length==0) { 
    alert("Error: Empty fields (X)");
    return false;
  }

  return true;
}

function displace(event) {
      switch (event.code) {
        case "KeyA":
          move_put('left');
          break;
        case "KeyW":
          move_put('up');
          break;
        case "KeyD":
          move_put('right');
          break;
        case "KeyS":
          move_put('down');
          break;
        }
}

//Change cell color according to its value
    function changeCellColor(){
      var cell;
      var idCol; 
      var idRow; 
      var idCell
      var row=4; 
      var col=4;
      for (i=0;i<row;i++) {
        switch (i) {
          case 0:
            idRow = 'cero'; break;
          case 1:
            idRow = 'uno'; break;
          case 2:
            idRow = 'dos'; break;
          case 3:
            idRow = 'tres';
        }
        for (j=0;j<col;j++) {
          idCol = j;
          idCell = idRow+idCol;
          cell = document.getElementById(idCell);
          switch (cell.innerHTML) {
            case '2':
              cell.setAttribute("style", "background-color: #eee4da; color: #776e65"); break;
            case '4':
              cell.setAttribute("style", "background-color: #ede0c8; color: #776e65"); break;
            case '8':
              cell.setAttribute("style", "background-color: #f2b179; color: #f9f6f2"); break;
            case '16':
              cell.setAttribute("style", "background-color: #f59563; color: #f9f6f2"); break;
            case '32':
              cell.setAttribute("style", "background-color: #f67c5f; color: #f9f6f2"); break;
            case '64':
              cell.setAttribute("style", "background-color: #f65e3b; color: #f9f6f2"); break;
            case '128':
              cell.setAttribute("style", "background-color: #edcf72; color: #f9f6f2"); break;
            case '256':
              cell.setAttribute("style", "background-color: #e5b834; color: #f9f6f2"); break;
            case '512':
              cell.setAttribute("style", "background-color: #cc9900; color: #f9f6f2"); break;
            case '1024':
              cell.setAttribute("style", "background-color: #997300; color: #f9f6f2"); break;
            case '2048':
              cell.setAttribute("style", "background-color: #00cc44; color: #f9f6f2"); break;
            default:
              cell.style.backgroundColor = "#cdc1b4"; break;
          } 
        }
      }
    }