#!/usr/bin/env python

"""
Very simple HTTP server in python.
Usage::
    ./webserver.py [<port>]
Send a GET request::
    curl http://localhost
Send a HEAD request::
    curl -I http://localhost
Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost
"""
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import json

class myServer(BaseHTTPRequestHandler):
    def content_type(self, path):
        if path.endswith(".css"):
            return "text/css"
        if path.endswith(".html"):
            return "text/html"
        if path.endswith(".js"):
            return "text/javascript"
        if path.endswith(".ico") or path.endswith(".png"):
            return "image/png"
        
        return "application/json"


    def _set_headers(self, status, type, content_length):
        self.send_response(status)
        if status == 200:
            self.send_header('Content-type', type)  
            self.send_header('Content-length', content_length)
        self.end_headers()

    def do_GET(self):
        print(self.command + " " + self.path + " " + self.request_version)
        print(self.headers)
        p= True

        if self.path == "/":
            self.path = "/views/signin.html"

        if self.path == "/sign_in":
            self.path = "/views/signin.html"
        
        if self.path == "/sign_up":
            self.path = "/views/signup.html"

        if self.path == "/data":
            datos = {
                "record" : 12,
                "savedGame" :  True, 
                }
            json_str = json.dumps(datos)
            self._set_headers(200, 'application/json', len(json_str))
            self.wfile.write(json_str)
            return
            
        if self.path == "/load":
            datos = {
                "record" : 100,
                "board" : [[0,0,0,0],[0,0,0,0],[0,2,0,0],[0,0,0,1024]]
                }
            json_str = json.dumps(datos)
            self._set_headers(200, 'application/json', len(json_str))
            self.wfile.write(json_str)
            return
            
        if self.path == "/home":
            self.path = "/views/home.html"

        if self.path == "/newgame":
            self.path = "/views/newgame.html"

        
        try:
            
            data = open(self.path[1:]).read()
            self._set_headers(200, self.content_type(self.path[1:]), len(data))
            self.wfile.write(data)
        except:
            self._set_headers(404, "text/plain", 0)
            # self.wfile.write("Error")

    def do_HEAD(self):
        self._set_headers('text/html', 200, 0)

    def do_POST(self):
        print(self.command + " " + self.path + " " + self.request_version)
        print(self.headers)

        if self.path == "/sign_in":
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            self.send_response(200)
            self.end_headers()
            print(body);
            a, b = json.dumps(body, sort_keys=True), json.dumps("{\"username\":\"asd\",\"password\":\"asd\"}", sort_keys=True)
            if a == b:
                datos = {
                    "isRegistered" : True,
                }
                json_str = json.dumps(datos)
                self.wfile.write(json_str)
            else:
                try:
                    datos = {
                        "isRegistered" : False,
                    }
                    json_str = json.dumps(datos)
                    self.wfile.write(json_str)
                except:
                    self._set_headers(404, "text/plain", 5)
                    self.wfile.write("")


        if self.path == "/sign_up":
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            self.send_response(200)
            self.end_headers()
            a, b = json.dumps(body, sort_keys=True), json.dumps("{\"username\":\"sofialavedia\",\"email\":\"sofialavedia@gmail.com\",\"password\":\"Sofia1234\"}", sort_keys=True)
            if a == b:
                datos = {
                    "isRegistered" : False,   
                    "error" : "Existing username"
                }
                json_str = json.dumps(datos)
                self.wfile.write(json_str)
            else:
                a, b = json.dumps(body, sort_keys=True), json.dumps("{\"username\":\"martindaniotti\",\"email\":\"martindaniotti@gmail.com\",\"password\":\"Martin45\"}", sort_keys=True)                
                if a == b:
                    datos = {
                        "isRegistered" : False,
                        "error" : "Existing email"
                    }
                    json_str = json.dumps(datos)
                    self.wfile.write(json_str)
                else:
                    try:
                        datos = {
                            "isRegistered" : True,
                            "error" : "---"
                        }
                        json_str = json.dumps(datos)
                        self.wfile.write(json_str)
                    except:
                        self._set_headers(404, "text/plain", 5)
                        self.wfile.write("")


        if self.path == "/newgame":
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            self.send_response(200)
            self.end_headers()
            datos = {
                "board" : [[0,0,0,0],[0,0,0,2],[0,0,0,0],[0,0,2,0]]
            }
            json_str = json.dumps(datos)
            self.wfile.write(json_str)

        if self.path == "/log_out":
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            self.send_response(200)
            self.end_headers()
            datos = {
                "closed":True
            }
            json_str = json.dumps(datos)
            self.wfile.write(json_str)

        if self.path == "/exit":
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            self.send_response(200)
            self.end_headers()
            datos = {}
            json_str = json.dumps(datos)
            self.wfile.write(json_str)

    def do_PUT(self):
        print(self.command + " " + self.path + " " + self.request_version)
        print(self.headers)

        if self.path == "/save":
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            self.send_response(200)
            self.end_headers()
            datos = {}
            json_str = json.dumps(datos)
            self.wfile.write(json_str)


        if self.path == "/move":
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            self.send_response(200)
            self.end_headers()
            a, b = json.dumps(body, sort_keys=True), json.dumps("{\"action\":\"up\"}", sort_keys=True)                
            if a == b:
                datos = {
                    "board" : [[2048,1024,512,256],[128,64,32,16],[8,4,2,2],[0,0,0,0]],
                    "score" : 10,
                    "record" : 100,
                    "lost": False
                }
                json_str = json.dumps(datos)
                print(json_str);
                self.wfile.write(json_str)
            else:
                a, b = json.dumps(body, sort_keys=True), json.dumps("{\"action\":\"down\"}", sort_keys=True)                
                if a == b:
                    datos = {
                        "board" : [[0,0,0,0],[2048,1024,512,256],[128,64,32,16],[8,4,2,2]],
                        "score" : 10,
                        "record" : 100,
                        "lost": False
                    }
                    json_str = json.dumps(datos)
                    self.wfile.write(json_str)
                else:
                    a, b = json.dumps(body, sort_keys=True), json.dumps("{\"action\":\"left\"}", sort_keys=True)                
                    if a == b:
                        datos = {
                            "board" : [[0,0,0,0],[2048,1024,512,256],[128,64,32,16],[8,4,2,2]],
                            "score" : 10,
                            "record" : 100,
                            "lost": False
                        }
                        json_str = json.dumps(datos)
                        self.wfile.write(json_str)
                    else:
                        a, b = json.dumps(body, sort_keys=True), json.dumps("{\"action\":\"right\"}", sort_keys=True)                
                        if a == b:
                            datos = {
                                "board" : [[0,0,0,0],[2048,1024,512,256],[128,64,32,16],[8,4,2,2]],
                                "score" : 10,
                                "record" : 100,
                                "lost": False
                            }
                            json_str = json.dumps(datos)
                            self.wfile.write(json_str)
                        try:
                            self.wfile.write("")
                        except:
                            self._set_headers(404, "text/plain", 5)
                            self.wfile.write("")
            
def run(server_class=HTTPServer, handler_class=myServer, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:  
        run(port=int(argv[1]))
    else:
        run()
