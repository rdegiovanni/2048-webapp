/*
* This file is the controller related to the play part, 
* which is used to contain all the functions that have to do with the play section
*/

/*
* This function is responsible of redirecting us to view home.html
*/
function home_get(){
  window.location = (ROUTE + "/home");
}

/*
* This function is responsible for sending a request to the server 
* to close the session and save the user's game, 
* or close the session and not save the game, according to what the user wants.
*/
function exit_post() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var out = confirm("Do you want log out?");
      if (out){
        save_put();
        log_out_get();
      }
    }else{
      if (this.readyState == 4 && this.status == 403) {
        alert("the user has not sign in");
      }
    }
  };
  xhttp.open("POST", ROUTE+"/exit", true);
  xhttp.send();
}

/*
* This function is responsible of refreshing the board, the score and the record
*/
function refresh_home(obj){
  var row=4;
  var col=4;
  for (i=0;i<row;i++) {
    var idRow;
    switch (i) {
      case 0:
        idRow = 'cero';
        break;
      case 1:
				idRow = 'uno';
				break;
			case 2:
				idRow = 'dos';
				break;
			case 3:
				idRow = 'tres';
		}
		for (j=0;j<col;j++) {
			var idCol = j;
			var idElement = idRow+idCol;
			if (obj.board[i][j] == 0)
				document.getElementById(idElement).innerHTML = "&nbsp;";
			else
				document.getElementById(idElement).innerHTML = obj.board[i][j];
		}
	}
	if (obj.score)
		document.getElementById('score').innerHTML = obj.score;
  else
    document.getElementById('score').innerHTML = 0;
	if (obj.record)
		document.getElementById('best').innerHTML = obj.record;
}

/*
* This function is responsible of sending a request to the server 
* to generate a new game to the user
*/
function newgame_post(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var obj = JSON.parse(this.responseText);
      refresh_home(obj);
    } else {
      if (this.readyState == 4 && this.status == 403)
        alert("the user has not sign in");
    }
  };
  xhttp.open("POST", ROUTE+"/newgame", true);
  xhttp.send();
}

/*
* This function is responsible of sending a request to the server 
* to know if the user has a saved game, 
* and to create a new game or bring the saved game depending on the response
*/
function data_get() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
  	  var obj = JSON.parse(this.responseText);
      if (obj.savedGame)
        load_get();
      else
        newgame_post();
    } else {
      if (this.readyState == 4 && this.status == 403)
        alert("the user has not sign in");
    }
  };
  xhttp.open("GET", ROUTE+"/data", true);
  xhttp.send();
}

/*
* This function is responsible of sending a request to the server 
* in order to bring the saved game to the user
*/
function load_get() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var obj = JSON.parse(this.responseText);
      refresh_home(obj);
    }else {
      if (this.readyState == 4 && this.status == 401) {
        alert("the user has not a saved game");
      }else {
        if(this.readyState == 4 && this.status == 403 ) {
          alert("the user has not sign in");
        }
      }
    }
  };
  xhttp.open("GET", ROUTE+"/load", true);
  xhttp.send();
}

/*
* This function is responsible of sending a request to the server to 
* save the game to the user
*/
function save_put() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      alert("Saved game");
    } else {
      if (this.readyState == 4 && this.status == 403)
        alert("The user has not sign in");
    }
  };
  var save = confirm("Confirm Save Game");
  if(save){
    xhttp.open("PUT", ROUTE+"/save", true);
    xhttp.send();
  }
}

/*
* This function is responsible of sending a request to the server to 
make a movement on the board
*/
function move_put(move) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
  if (xhr.readyState == 4 && xhr.status == 200) {
    var obj = JSON.parse(this.responseText);
    if (obj.lost){
      refresh_home(obj);
      var mensaje = confirm("GAME OVER");
      if (mensaje)
        newgame_post();
      else
        exit_post();
    }
    else
      refresh_home(obj);
  } else {
      if (this.readyState == 4 && this.status == 403)
        alert("the user has not sign in");
      else {
        if (this.readyState == 4 && this.status == 405)
         alert("the game is lost");
      }
    }
  };
  var obj = JSON.stringify({action: move});
  xhr.open("PUT", ROUTE+"/move", true);
  xhr.send(obj);
}
