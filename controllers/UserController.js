const ROUTE = "http://localhost:8080";

function sign_in_get() {
  window.location = (ROUTE + "/sign_in");
}

function sign_up_get() {
  window.location = (ROUTE + "/sign_up");
}

function sign_in_post() {
  var username = document.getElementById('username').value;
	var password = document.getElementById('password').value;
	var xhttp = new XMLHttpRequest();
	var object = {"username":username,"password":password};
  xhttp.onreadystatechange = function() {
  	if (this.readyState == 4 && this.status == 200) {
      var obj = JSON.parse(this.responseText);
      if (obj.isRegistered)
     		home_get();
     	else
     		alert("could not log in");

  	}
	};
  xhttp.open("POST", ROUTE+"/sign_in", true);
 	xhttp.send(JSON.stringify(object));
}

function sign_up_post() {
  var username = document.getElementById('username').value;
  var email = document.getElementById('email').value;
  var password = document.getElementById('password').value;
  var pass_confirmation = document.getElementById('pass_confirmation').value;
  var xhttp = new XMLHttpRequest();
  var objReq = {"username":username, "email":email, "password":password};
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var objRes = JSON.parse(this.responseText);
      if (objRes.isRegistered)
        sign_in_get();
      else {
        if (objRes.error == "Existing username")
          document.getElementById("message_exist_user").style.display="block";
        else {
          if (objRes.error == "Existing email")
            document.getElementById("message_exist_email").style.display="block";
          else
            alert("An error has ocurred");
        }
      }
    }
  };
  xhttp.open("POST", ROUTE+"/sign_up");
  xhttp.send(JSON.stringify(objReq));
}

function log_out_get() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var obj = JSON.parse(this.responseText);
      if (obj.closed)
        sign_in_get();
      else
        alert("could not log out");
    } else {
      if (this.readyState == 4 && this.status == 403)
        alert("could not log out");
    }
  };
  xhttp.open("POST", ROUTE+"/log_out", true);
  xhttp.send();
}

window.onload = function() {
  if (window.location.href === (ROUTE + "/home"))
    data_get();
};
